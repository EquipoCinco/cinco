package controlador;

/**
 *
 * @author Fabiola
 */
public class Cuate {
    private String user,pwd,apellidos,nombres;
    private double cuotas;
    
    public Cuate(String user,String pwd , String apellidos,String nombres ,double cuotas){
        this.user=user;
        this.pwd=pwd;
        this.nombres=nombres;
        this.apellidos=apellidos;
        this.cuotas=cuotas;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the pwd
     */
    public String getPwd() {
        return pwd;
    }

    /**
     * @param pwd the pwd to set
     */
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    /**
     * @return the apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * @param apellidos the apellidos to set
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * @return the nombres
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * @param nombres the nombres to set
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * @return the cuotas
     */
    public double getCuotas() {
        return cuotas;
    }

    /**
     * @param cuotas the cuotas to set
     */
    public void setCuotas(double cuotas) {
        this.cuotas = cuotas;
    }
}
