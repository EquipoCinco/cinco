/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Fabiola
 */
@ManagedBean(name="buscaCuates",eager=true)
@ApplicationScoped
public class MapeaCuates implements CuateBuscaInterface {
    private Map<String,Cuate> cuates;
    private Collection <Cuate> lista=new ArrayList<Cuate>();
    public MapeaCuates()
    {
        //intruso no se encuentra
        //<100 deudor
        //cualquiera => cuate
        cuates=new HashMap<>();      
        System.out.println("Creando cuates");
        addCuate(new Cuate("patito","asdf12","martinez perez","jesus",1254.2));
        addCuate(new Cuate("vaca12","asdf13","aguilar avila","pedro",234.2));
        addCuate(new Cuate("pollito","asdf14","hernandez juarez","frabicio",234.2));
        addCuate(new Cuate("gatito","asdf15","sanchez agustin","karla",9.2));//deudor
        addCuate(new Cuate("perrito","asdf16","solis roman","maria",234.2));
    }
    private void addCuate(Cuate cuate){
        System.out.println("Cuotas: "+ cuate.getCuotas());
        lista.add(cuate);   
        cuates.put(cuate.getUser(),cuate);
    }
    
    @Override
    public Cuate BuscaCuate(String login,String pwd){
        Cuate cuate=null;
        if(login!=null){
            cuate=cuates.get(login.toLowerCase());
            if(cuate!=null){
                if(cuate.getPwd().equals(pwd)){
                    return cuate;
                }
                else
                return (null);
            }
        }
        return cuate;
    }

    @Override
    public Collection<Cuate> listaCuates() {
        return lista ;
    }
}
