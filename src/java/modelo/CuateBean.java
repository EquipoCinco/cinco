
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.*;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Fabiola
 */
@ManagedBean(name = "cuate")
@SessionScoped
public class CuateBean {
    
    private String user,password;
    private Cuate cuate;    
    @ManagedProperty(value="#{buscaCuates}")
    private CuateBuscaInterface cuates;
    private Collection <Cuate> listaCuates;

    public Collection<Cuate> getListaCuates() {
        listaCuates=cuates.listaCuates();
        return listaCuates;
    }
    
    public CuateBean() {
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
    public String pagoCuotas(){
        String pagina="";
        setCuate(getCuates().BuscaCuate(user, password));
        if (getCuate()==null){
            pagina="Intruso";
        }else if (getCuate().getCuotas()<100){
            pagina="Deudor";
        }else{
            pagina="ListaCuates";
        }
        return pagina;
    }

    /**
     * @return the cuate
     */
    public Cuate getCuate() {
        return cuate;
    }

    /**
     * @param cuate the cuate to set
     */
    public void setCuate(Cuate cuate) {
        this.cuate = cuate;
    }

    /**
     * @return the cuates
     */
    public CuateBuscaInterface getCuates() {
        return cuates;
    }

    /**
     * @param cuates the cuates to set
     */
    public void setCuates(CuateBuscaInterface cuates) {
        this.cuates = cuates;
    }
}
